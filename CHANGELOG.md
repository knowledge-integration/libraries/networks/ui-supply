## [1.4.2](https://gitlab.com/knowledge-integration/libraries/networks/ill-supply/compare/v1.4.1...v1.4.2) (2023-11-02)

## [1.4.1](https://gitlab.com/knowledge-integration/libraries/networks/ill-supply/compare/v1.4.0...v1.4.1) (2023-10-21)


### Bug Fixes

* **build:** Add missing dev deps ([757c3d9](https://gitlab.com/knowledge-integration/libraries/networks/ill-supply/commit/757c3d92a528bdb02631919abbc993c843995576))
* **build:** Updated CI script to publish Module Descriptor ([993ce32](https://gitlab.com/knowledge-integration/libraries/networks/ill-supply/commit/993ce329510085bee98fdcbb9231182c11202e91))
* **build:** Updated CI script to publish Module Descriptor ([b9e988d](https://gitlab.com/knowledge-integration/libraries/networks/ill-supply/commit/b9e988d5d9078c1de7eb939e5bda585cdfb172e1))
* **build:** updated package.json scripts and added .releaserc.json ([45f8516](https://gitlab.com/knowledge-integration/libraries/networks/ill-supply/commit/45f8516930ee3ec051662cc59b40744b583bf0d7))

# Change history for ui-supply

## [1.0.0](https://github.com/openlibraryenvironment/ui-supply/tree/v1.0.0) (2020-08-24)

* Initial release, belatedly. This file didn't get created until 2020-09-29, but since a `v1.0.0` branch already exists on GitHub, it's most likely that this was done at the same time that all the `v1.0.0` tags were created for other Ill modules, on 2020-08-24.
